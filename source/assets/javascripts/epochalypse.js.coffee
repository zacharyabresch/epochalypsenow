class window.Epochalypse
  constructor: () ->
    [@$timestamp_target, @$start_button, @$stop_button, @interval] = 
      [$(".timestamper"), $(".start"), $(".stop"), null]
    @.register_handlers()

  set_html: () -> @$timestamp_target.html Date.now()

  start_timer: () ->
    @interval = setInterval( => 
      @.set_html()
    , 1000)

  stop_timer: () -> clearInterval(@interval) if @interval

  register_handlers: () ->
    @$start_button.on "click", => @.start_timer()
    @$stop_button.on "click", => @.stop_timer()