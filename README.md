# Epochalypse Now!!!

I built a little tool to display the current UNIX timestamp *and* to be able to start a timer to display new timestamps. I needed something like this for work and wanted to play around with [Middleman][1] and [Proteus][1].

Want to mess with it? Just [fork it][3] and play on playa!

[1]:https://middlemanapp.com/
[2]:https://github.com/thoughtbot/proteus-middleman
[3]:https://bitbucket.org/zacharyabresch/epochalypsenow/fork